import "./jquery-3.4.1.min.js"

window.onload = function() {
    console.log("a")
    $("#loading-screen").fadeOut(500, "swing", function() {
        $(this).remove()
    })
}

window.onscroll = function() {
    if (isScrolledIntoView($(".cube"))) {
        $(".cube").addClass("show")
    } else {
        $(".cube").removeClass("show")
    }
    let moved = 1 - ($(window).scrollTop() / window.innerHeight)
    moved = Math.max(moved, 0)
    if (moved <= 0) {
        $("#top-bar").addClass("show")
    } else {
        $("#top-bar").removeClass("show")
    }
    $("header").children().css("opacity", ""+moved)
    if (moved >= 1) {
        $("header>a").css("pointer-events", "all")
    } else {
        $("header>a").css("pointer-events", "none")
    }
}

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}